import fontawesome from '@fortawesome/fontawesome'
import { faChevronCircleRight } from '@fortawesome/fontawesome-free-solid'

fontawesome.config = {
  autoAddCss: false,
}

fontawesome.library.add(faChevronCircleRight)

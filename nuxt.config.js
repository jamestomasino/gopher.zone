module.exports = {
  head: {
    title: 'gopher.zone',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Gopher Lives' },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
      },
    ],
  },
  css: ['assets/main.css', '@fortawesome/fontawesome/styles.css'],
  loading: { color: '#3B8070' },
  plugins: ['~/plugins/font-awesome'],
  build: {
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
  generate: {
    fallback: true,
  },
  modules: ['nuxt-buefy'],
  buefy: {
    materialDesignIcons: false,
  },
}
